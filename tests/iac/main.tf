module "random_pet" {
  source = "./modules/random-pet"
}

resource "local_file" "foo" {
  content  = "foo!"
  filename = "${path.module}/foo.bar"
}

variable "CI_PROJECT_NAME" {
  type    = string
  default = "default"
}

output "project_name" {
  value = var.CI_PROJECT_NAME
}
